<?php

	function city_selector( $atts ){
		ob_start();
		?>

			<div id="hsc-city-selector" class="mt-4" style="padding-bottom: 20px;">
				<script src="https://cdn.jsdelivr.net/npm/jquery-selectric@0.1.3/src/jquery.selectric.min.js"></script>

				<style>
					#hsc-city-selector .city-selector-main{
						padding: 40px 30px 50px 30px;
						background-color: #069;
					}

					#hsc-city-selector h4{
						color: #fff;
						margin-bottom: 15px;
					}
				</style>

				<div class="city-selector-main">
					<h2 class="mb-5 font-weight-bold text-white" style="text-align: center; margin-bottom: 20px;">Looking to build or renovate select a location</h2>
					<div class="row justify-content-center">
						<div class="col-sm-6 col-sm-offset-3 hidden">
							<select id="province-select">
							</select>
						</div>
						<div class="col-sm-6 col-sm-offset-3">
							<?php
                            //print_r(get_sites());
                            //die;
                            ?>
							<select class="select" id="city-select">
								<option value="">CHOOSE A LOCATION</option>
								<?php
								$sub_sites = get_sites();
								$site_url  = get_site_url();

								foreach ( $sub_sites as $sub_site ) {
									$sub_site_id   = $sub_site->blog_id;
								    //dont add deactivated sites
                                    if($sub_site->deleted == 1){
                                        continue;
                                    }
									//skip the main site
									if ( $sub_site->path == '/') {
									    continue;
                                    }

									//only add if site is public
									if($sub_site->public != 1){
										continue;
									}

									//only add if the 'Show In Locations DropDown' option is checked
                                    $show_in_locations_dropdown = get_blog_option($sub_site_id, 'show_in_locations_dropdown');
                                    if($show_in_locations_dropdown == false){
	                                    continue;
                                    }


									/*if ( $sub_site->path == '/' ||
                                        $sub_site->path == '/custom-home-builder' ||
                                        $sub_site->path == '/laneway-homes' ||
                                        $sub_site->path == 'home-renovations'
                                    ) {
										continue;
									}*/

									$sub_site_name = str_replace( "Homes of Silvercrest", "", get_blog_details( $sub_site_id )->blogname );
									echo '<option value="https://homesofsilvercrest.com' . $sub_site->path . '">' . $sub_site_name . '</option>';
								}
								?>
							</select>
						</div>
					</div>

				</div>

				<script>

					(function($) {
					    $(document).ready(function(){
					    	$('#hsc-city-selector select').selectric({
					    		disableOnMobile: false,
					    		nativeOnMobile: false
					    	});

					    	$('#hsc-city-selector #city-select').on('change', function(){
					    		if($(this).val() != ''){
					    			window.location.assign($(this).val());
					    		}
					    	});
					    });
					}(jQuery));

				</script>
			</div>

		<?php
		return ob_get_clean();
	}
	add_shortcode( 'city-selector', 'city_selector' );

    function lg_custom_post_type(){
      register_post_type( 'location',
          array(
            'labels' => array(
              'name' => __( 'Locations' ),
              'singular_name' => __( 'Location' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location'
          )
      );
    }

    add_action( 'init', 'lg_custom_post_type' );

    function lg_custom_taxonomy(){

      register_taxonomy(
        'location-category',
        'location',
        array(
          'label' => __( 'Category' ),
          'rewrite' => array( 'slug' => 'location-category' ),
          'hierarchical' => true,
        )
      );
    }

    add_action( 'init', 'lg_custom_taxonomy' );

?>