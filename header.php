<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package homebuilder
 */

// Gathering data for header 
$logo            = homebuilder_get_option( 'logo_default' );
$time            = homebuilder_get_option( 'topbar_available_time' );
$email           = homebuilder_get_option( 'topbar_email_addr' );
$phone           = homebuilder_get_option( 'topbar_phone_no' );
$layout          = homebuilder_get_option( 'header_layout', 'default' );
$retina          = homebuilder_get_option( 'logo_retina' );
$tagline         = get_bloginfo( 'description' );
$preloader       = homebuilder_get_option( 'display_preloader' );
$has_social      = homebuilder_get_option( 'display_topbar_social', true );
$has_topbar      = homebuilder_get_option( 'has_topbar', true );
$has_tagline     = homebuilder_get_option( 'has_tagline', true );
$tagline_content = homebuilder_get_option( 'topbar_tagline_content', $tagline );
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="P9g4o2LY0hp2Xof_LEqTks7lc2l1BvtchG1N4Kuk2hM" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body itemscope itemtype="https://schema.org/WebPage" <?php body_class(); ?>>
    <?php 
    if ( $preloader ) {
        homebuilder_preloader();
    }
    //Set the value of public query variable, so header layout can retrieve those data
    set_query_var( 'header_data', compact('logo','time','email','phone','retina','tagline','has_topbar','has_social','has_tagline','tagline_content'));
    get_template_part( "partials/header/{$layout}" );
    ?>